package org.pan.entity;


import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class EntityObject {
    @Id
    @GeneratedValue
    private Long id;
}
