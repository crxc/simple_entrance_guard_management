package org.pan;

import io.advantageous.guicefx.JavaFXModule;

public class MyApplicationModule extends JavaFXModule {

    @Override
    protected void configureFXApplication() {
        //Do your Guice config here
    }
}